# Настройка кластера после развертывания виртуалок

## Инициализация master-node

```bash
    sudo kubeadm init --apiserver-advertise-address=192.168.66.11 --pod-network-cidr=10.244.0.0/16 --v=5
```

После успешной инифициализации 

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## Присоединение worker-node
``` bash
sudo kubeadm join 192.168.66.11:6443 .... --v=5

```

## Установка сетевого плагина
На master-node выполнить команду

```bash
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
```
