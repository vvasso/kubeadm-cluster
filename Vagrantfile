# -*- mode: ruby -*-
# vi:set ft=ruby sw=2 ts=2 sts=2:

# Define the number of master and worker nodes
# If this number is changed, remember to update setup-hosts.sh script with the new hosts IP details in /etc/hosts of each VM.
NUM_MASTER_NODE = 1
NUM_WORKER_NODE = 1

IP_NW = "192.168.66."
MASTER_IP_START = 1
NODE_IP_START = 2
LB_IP_START = 30


Vagrant.configure("2") do |config|

  config.ssh.insert_key = false
  config.vm.box_download_insecure = true
  config.vm.box = "generic/centos7"
  config.vm.box_check_update = false

  # Provision Master Nodes
  (1..NUM_MASTER_NODE).each do |i|
      config.vm.define "km-node0#{i}" do |node|
        node.vm.ignore_box_vagrantfile = true
        node.vm.provider "virtualbox" do |vb|
            vb.name = "km-node0#{i}"
            vb.memory = 2048
            vb.cpus = 2
            vb.check_guest_additions = false
            vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
            vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
            vb.customize ["modifyvm", :id, "--vram", "16"]
        end
        node.vm.hostname = "km-node0#{i}"
        node.vm.network :private_network, ip: IP_NW + "#{MASTER_IP_START}" + "#{i}"
        node.vm.network "forwarded_port", guest: 22, host: "223#{i}", id: "ssh"

        node.vm.provision "setup-hosts", :type => "shell", :path => "centos7/vagrant/setup-hosts.sh" do |s|
          s.args = ["eth1"]
        end

        node.vm.provision "setup-brnetfilter", type: "shell", :path => "centos7/vagrant/setup-brnetfilter.sh"
        node.vm.provision "setup-docker", type: "shell", :path => "centos7/vagrant/setup-docker.sh"
        node.vm.provision "install-kubernetes", type: "shell", :path => "centos7/vagrant/install-kubernetes.sh"
        

      end
  end

  # Provision Worker Nodes
  (1..NUM_WORKER_NODE).each do |i|
    config.vm.define "kw-node0#{i}" do |node|
        node.vm.ignore_box_vagrantfile = true
        node.vm.provider "virtualbox" do |vb|
            vb.name = "kw-node0#{i}"
            vb.memory = 2048
            vb.cpus = 2
            vb.check_guest_additions = false
            vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
            vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
            vb.customize ["modifyvm", :id, "--vram", "16"]
        end

        node.vm.hostname = "kw-node0#{i}"
        node.vm.network :private_network, ip: IP_NW + "#{NODE_IP_START}" + "#{i}"
        node.vm.network "forwarded_port", guest: 22, host: "224#{i}", id: "ssh"

        node.vm.provision "setup-hosts", :type => "shell", :path => "centos7/vagrant/setup-hosts.sh" do |s|
          s.args = ["eth1"]
        end

        node.vm.provision "setup-brnetfilter", type: "shell", :path => "centos7/vagrant/setup-brnetfilter.sh"
        node.vm.provision "setup-docker", type: "shell", :path => "centos7/vagrant/setup-docker.sh"
        node.vm.provision "install-kubernetes", type: "shell", :path => "centos7/vagrant/install-kubernetes.sh"
    end
  end

end 