#!/bin/bash

sudo yum install -y haproxy

cat <<EOF | sudo tee -a /etc/haproxy/haproxy.cfg
frontend kubernetes-frontend
    bind 192.168.66.30:6443
    mode tcp
    option tcplog
    default_backend kubernetes-backend

backend kubernetes-backend
    mode tcp
    option tcp-check
    balance roundrobin
    server controlplane01 192.168.66.11:6443 check fall 3 rise 2
    server controlplane02 192.168.66.12:6443 check fall 3 rise 2    
EOF

sudo sed -i '/$ModLoad imudp/s/^#//' /etc/rsyslog.conf
sudo sed -i '/$UDPServerRun 514/s/^#//' /etc/rsyslog.conf
sudo sed -i 's/log         127.0.0.1 local2/log         127.0.0.1 local2 info/g'  /etc/haproxy/haproxy.cfg

cat <<EOF | sudo tee /etc/rsyslog.d/haproxy.conf
    local2.*    /var/log/haproxy.log
EOF
sudo systemctl restart rsyslog


setsebool -P haproxy_connect_any=1
sudo systemctl enable haproxy
sudo systemctl start haproxy

# sudo firewall-cmd --permanent --add-port=6443/tcp
# sudo firewall-cmd --permanent --add-port=4331/tcp
# sudo firewall-cmd --reload
sudo systemctl stop firewalld
sudo systemctl disable firewalld


